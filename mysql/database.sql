DROP DATABASE IF EXISTS datalogger;
CREATE DATABASE datalogger;
USE datalogger;

CREATE TABLE arduino (
  id INT(2) NOT NULL AUTO_INCREMENT,
  name VARCHAR(50) NOT NULL,
  port VARCHAR(100) NOT NULL,
  description VARCHAR(200) DEFAULT NULL,
  PRIMARY KEY(id)
);

CREATE TABLE sensor (
  id INT(3) NOT NULL,
  name VARCHAR(50) NOT NULL,
  unit VARCHAR(20) NOT NULL,
  description VARCHAR(200) DEFAULT NULL,
  PRIMARY KEY(id)
);

CREATE TABLE logging (
  id INT(7) NOT NULL AUTO_INCREMENT,
  arduino INT NOT NULL,
  sensor INT NOT NULL,
  datumzeit DATETIME NOT NULL,
  value FLOAT NOT NULL,
  PRIMARY KEY(id),
  FOREIGN KEY (arduino) REFERENCES arduino(id),
  FOREIGN KEY (sensor) REFERENCES sensor(id)
);

CREATE TABLE arduino_sensor (
  id INT NOT NULL AUTO_INCREMENT,
  arduino INT NOT NULL,
  sensor INT NOT NULL,
  PRIMARY KEY(id),
  FOREIGN KEY (arduino) REFERENCES arduino(id),
  FOREIGN KEY (sensor) REFERENCES sensor(id)
);

INSERT INTO arduino (name, port, description)
VALUES  ("Cockpit", "/dev/ttyACM0", "Arduino UNO"),
        ("Kofferraum", "/dev/ttyACM1", "Arduino UNO"),
        ("Frontscheibe", "/dev/ttyACM2", "Arduino UNO");

INSERT INTO sensor (id, name, unit, description)
VALUES  (1, "Temperatur", "°C", "Temperatur und Feuchtigkeitssensor DHT22"),
        (2, "Feuchtigkeit", "%", "Temperatur und Feuchtigkeitssensor DHT22"),
        (3, "Staubkonzentration", "ppm", "Grove-Dust Sensor V1.0"),
        (4, "Sauerstoffgehalt", "%", "Gas Sensor (O2)"),
        (5, "CO2 Konzentration", "ppm", "Gas Sensor (CO2)"),
        (11, "Zweite Temperatur", "°C", "Zweiter Temperatur und Feuchtigkeitssensor DHT22"),
        (12, "Zweite Feuchtigkeit", "%", "Zweiter Temperatur und Feuchtigkeitssensor DHT22"),
        (90, "NH3 Konzentration", "ppm", "Multichannel Gas Sensor"),
        (91, "CO Konzentration", "ppm", "Multichannel Gas Sensor"),
        (92, "NO2 Konzentration", "ppm", "Multichannel Gas Sensor"),
        (93, "C3H8 Konzentration", "ppm", "Multichannel Gas Sensor"),
        (94, "C4H10 Konzentration", "ppm", "Multichannel Gas Sensor"),
        (95, "CH4 Konzentration", "ppm", "Multichannel Gas Sensor"),
        (96, "H2 Konzentration", "ppm", "Multichannel Gas Sensor"),
        (97, "C2H50H Konzentration", "ppm", "Multichannel Gas Sensor");

/*INSERT INTO logging (arduino, sensor, datumzeit, value)
VALUES  (1, 1, "2018-11-11 10:10:10", 25.3),
        (2, 1, "2018-11-11 10:10:20", 27.4),
        (1, 2, "2018-11-11 10:10:30", 12.0),
        (1, 1, "2018-11-11 10:10:40", 16.0),
        (3, 2, "2018-11-11 10:10:50", 25.6);*/

INSERT INTO arduino_sensor (arduino, sensor)
VALUES  (1, 1),
        (1, 2),
        (1, 5),
        (2, 1),
        (2, 2),
        (2, 3),
        (3, 1),
        (3, 2),
        (3, 4),
        (3, 11),
        (3, 12),
        (3, 90),
        (3, 91),
        (3, 92),
        (3, 93),
        (3, 94),
        (3, 95),
        (3, 96),
        (3, 97);
