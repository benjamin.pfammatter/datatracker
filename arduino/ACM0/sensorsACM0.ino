#define ARDUINO_ID 1 //ttyACM0

//TEMP- AND HUMIDITYSENSOR
#include "DHT.h"
#define DHTPIN 2     // what pin we're connected to
#define DHTTYPE DHT22   // DHT 22  (AM2302)
DHT dht(DHTPIN, DHTTYPE);
//TEMP- AND HUMIDITYSENSOR

//CO2 SENSOR
#include <SoftwareSerial.h>
const int pinRx = 9;
const int pinTx = 8;
SoftwareSerial sensor(pinTx,pinRx);
unsigned char flg_get = 0;              // if get sensor data
const unsigned char cmd_get_sensor[] = {
0xff, 0x01, 0x86, 0x00, 0x00, 0x00, 0x00, 0x00, 0x79};
bool sendCmdGetDta(int *gas_strength, int *temperature)
{
    for(int i=0; i<sizeof(cmd_get_sensor); i++)
    {
        sensor.write(cmd_get_sensor[i]);
    }
    long cnt_timeout = 0;
    while(!sensor.available())              // wait for data
    {
        delay(1);
        cnt_timeout++;

        if(cnt_timeout>1000)return 0;       // time out
    }
    int len = 0;
    unsigned char dta[20];
    while(sensor.available())
    {
        dta[len++] = sensor.read();
    }
    if((9 == len) && (0xff == dta[0]) && (0x86 == dta[1]))      // data ok
    {
        *gas_strength = 256*dta[2]+dta[3];
        *temperature = dta[4] - 40;

        return 1;
    }
    return 0;
}
//CO2 SENSOR
void setup()
{
    Serial.begin(9600);

    //TEMP- AND HUMIDITYSENSOR
    dht.begin();
    //TEMP- AND HUMIDITYSENSOR
}

void loop()
{
    //TEMP- AND HUMIDITYSENSOR
    float h = dht.readHumidity();
    float t = dht.readTemperature();
    if (isnan(t) || isnan(h))
    {
        Serial.println("ERROR");
    }
    else
    {
        //Format: ArduinoID,SensorID,SensorValue
        Serial.print(ARDUINO_ID);
        Serial.print(",1,");  //SensorID
        Serial.print(t);    //SensorValue in *C
        Serial.println(",");
        delay(1000);
        Serial.print(ARDUINO_ID);
        Serial.print(",2,");  //SensorID
        Serial.print(h);    //SensorValue in %
        Serial.println(",");
        delay(1000);
    }
    //TEMP- AND HUMIDITYSENSOR

    //CO2 SENSOR
    flg_get = 0;
    int gas, temp;
    if(sendCmdGetDta(&gas, &temp))          // get data ok
    {
        Serial.print(ARDUINO_ID);
        Serial.print(",5,");  //SensorID
        Serial.print(gas);    //SensorValue in ppm
        Serial.print(",");
        Serial.print(temp);
        Serial.println(",");
    }
    else
    {
        Serial.println("get data error");
    }
    delay(1000);
    //CO2 SENSOR
    delay(600000);
}
