#define ARDUINO_ID 3 //ttyACM2

//TEMP- AND HUMIDITYSENSOR
#include "DHT.h"
#define DHTPIN 2     // D2 connector
#define DHTTYPE DHT22   // DHT22 (AM2302)
DHT dht(DHTPIN, DHTTYPE);
//TEMP- AND HUMIDITYSENSOR

//TEMP- AND HUMIDITYSENSOR
#define DHTPIN_2 3     // D2 connector
#define DHTTYPE_2 DHT22   // DHT22 (AM2302)
DHT dht2(DHTPIN_2, DHTTYPE_2);
//TEMP- AND HUMIDITYSENSOR

//SAUSERSTOFFSENSOR
const float O2_VRefer = 5;       // voltage of arduino sensors adapter
const int O2_pinAdc   = A1;
float readO2Vout()
{
    long sum = 0;
    for(int i=0; i<32; i++)
    {
        sum += analogRead(O2_pinAdc);
    }
    sum >>= 5;
    float MeasuredVout = sum * (O2_VRefer / 1023.0);
    return MeasuredVout;
}
float readConcentration()
{
    float MeasuredVout = readO2Vout();
    float Concentration = MeasuredVout * 0.21 / 2.0;
    float Concentration_Percentage=Concentration*100;
    return Concentration_Percentage;
}
//SAUSERSTOFFSENSOR

//MULTICHANNEL GAS SENSOR
#include <Wire.h>
#include "MutichannelGasSensor.h"
//MULTICHANNEL GAS SENSOR

void setup()
{
    Serial.begin(9600);

    //TEMP- AND HUMIDITYSENSOR
    dht.begin();
    //TEMP- AND HUMIDITYSENSOR

    //TEMP- AND HUMIDITYSENSOR
    dht2.begin();
    //TEMP- AND HUMIDITYSENSOR

    //MULTICHANNEL GAS SENSOR
    gas.begin(0x04);    //the default I2C address of the slave is 0x04
    gas.powerOn();
    //MULTICHANNEL GAS SENSOR
}

void loop()
{
    //TEMP- AND HUMIDITYSENSOR
    float h = dht.readHumidity();
    float t = dht.readTemperature();
    if (isnan(t) || isnan(h))
    {
        Serial.println("ERROR");
    }
    else
    {
        //Format: ArduinoID,SensorID,SensorValue
        Serial.print(ARDUINO_ID);
        Serial.print(",1,");  //SensorID
        Serial.print(t);    //SensorValue in *C
        Serial.println(",");
        delay(1000);
        //Format: ArduinoID,SensorID,SensorValue
        Serial.print(ARDUINO_ID);
        Serial.print(",2,");  //SensorID
        Serial.print(h);    //SensorValue in %
        Serial.println(",");
        delay(1000);
    }
    //TEMP- AND HUMIDITYSENSOR

    //TEMP- AND HUMIDITYSENSOR
    h = dht2.readHumidity();
    t = dht2.readTemperature();
    if (isnan(t) || isnan(h))
    {
        Serial.println("ERROR");
    }
    else
    {
        //Format: ArduinoID,SensorID,SensorValue
        Serial.print(ARDUINO_ID);
        Serial.print(",11,");  //SensorID
        Serial.print(t);    //SensorValue in *C
        Serial.println(",");
        delay(1000);
        //Format: ArduinoID,SensorID,SensorValue
        Serial.print(ARDUINO_ID);
        Serial.print(",12,");  //SensorID
        Serial.print(h);    //SensorValue in %
        Serial.println(",");
        delay(1000);
    }
    //TEMP- AND HUMIDITYSENSOR

    //SAUSERSTOFFSENSOR
    float O2_Vout = 0;
    O2_Vout = readO2Vout();
    float O2_percentage = 0;
    O2_percentage = readConcentration();
    //Format: ArduinoID,SensorID,SensorValue,SensorVoltage
    Serial.print(ARDUINO_ID);
    Serial.print(",4,");  //SensorID
    Serial.print(O2_percentage);    //SensorValue in %
    Serial.print(",");
    Serial.print(O2_Vout);
    Serial.println(",");
    delay(1000);
    //SAUSERSTOFFSENSOR

    //MULTICHANNEL GAS SENSOR
    float c;
    c = gas.measure_NH3();
    //Format: ArduinoID,SensorID,SensorValue
    Serial.print(ARDUINO_ID);
    Serial.print(",90,");  //SensorID
    if(c>=0) Serial.print(c);    //SensorValue in ppm
    else Serial.print("99.99");
    Serial.println(",");
    delay(1000);
    c = gas.measure_CO();
    //Format: ArduinoID,SensorID,SensorValue
    Serial.print(ARDUINO_ID);
    Serial.print(",91,");  //SensorID
    if(c>=0) Serial.print(c);    //SensorValue in ppm
    else Serial.print("invalid");
    Serial.println(",");
    delay(1000);
    c = gas.measure_NO2();
    //Format: ArduinoID,SensorID,SensorValue
    Serial.print(ARDUINO_ID);
    Serial.print(",92,");  //SensorID
    if(c>=0) Serial.print(c);    //SensorValue in ppm
    else Serial.print("invalid");
    Serial.println(",");
    delay(1000);
    c = gas.measure_C3H8();
    //Format: ArduinoID,SensorID,SensorValue
    Serial.print(ARDUINO_ID);
    Serial.print(",93,");  //SensorID
    if(c>=0) Serial.print(c);    //SensorValue in ppm
    else Serial.print("invalid");
    Serial.println(",");
    delay(1000);
    c = gas.measure_C4H10();
    //Format: ArduinoID,SensorID,SensorValue
    Serial.print(ARDUINO_ID);
    Serial.print(",94,");  //SensorID
    if(c>=0) Serial.print(c);    //SensorValue in ppm
    else Serial.print("invalid");
    Serial.println(",");
    delay(1000);
    c = gas.measure_CH4();
    //Format: ArduinoID,SensorID,SensorValue
    Serial.print(ARDUINO_ID);
    Serial.print(",95,");  //SensorID
    if(c>=0) Serial.print(c);    //SensorValue in ppm
    else Serial.print("invalid");
    Serial.println(",");
    delay(1000);
    c = gas.measure_H2();
    //Format: ArduinoID,SensorID,SensorValue
    Serial.print(ARDUINO_ID);
    Serial.print(",96,");  //SensorID
    if(c>=0) Serial.print(c);    //SensorValue in ppm
    else Serial.print("invalid");
    Serial.println(",");
    delay(1000);
    c = gas.measure_C2H5OH();
    //Format: ArduinoID,SensorID,SensorValue
    Serial.print(ARDUINO_ID);
    Serial.print(",97,");  //SensorID
    if(c>=0) Serial.print(c);    //SensorValue in ppm
    else Serial.print("invalid");
    Serial.println(",");
    delay(1000);

    delay(590000);
}
