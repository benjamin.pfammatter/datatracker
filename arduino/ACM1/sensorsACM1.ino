#define ARDUINO_ID 2 //ttyACM1

//TEMP- AND HUMIDITYSENSOR
#include "DHT.h"
#define DHTPIN 2     // D2 connector
#define DHTTYPE DHT22   // DHT22  (AM2302)
DHT dht(DHTPIN, DHTTYPE);
//TEMP- AND HUMIDITYSENSOR

//DUST SENSOR
int dustPin = 8; // D8 connector
unsigned long dustDuration;
unsigned long dustStarttime;
unsigned long dustSampletime_ms = 30000; //sample 30s ;
unsigned long dustLowpulseoccupancy = 0;
float dustRatio = 0;
float dustConcentration = 0;
//DUST SENSOR

void setup()
{
    Serial.begin(9600);

    //TEMP- AND HUMIDITYSENSOR
    dht.begin();
    //TEMP- AND HUMIDITYSENSOR

    //DUST SENSOR
    pinMode(dustPin,INPUT);
    dustStarttime = millis();//get the current time;
    //DUST SENSOR
}

void loop()
{
    //TEMP- AND HUMIDITYSENSOR
    float h = dht.readHumidity();
    float t = dht.readTemperature();
    if (isnan(t) || isnan(h))
    {
        Serial.println("ERROR");
    }
    else
    {
        //Format: ArduinoID,SensorID,SensorValue
        Serial.print(ARDUINO_ID);
        Serial.print(",1,");  //SensorID from database
        Serial.print(t);    //SensorValue in *C
        Serial.println(",");
        delay(1000);
        //Format: ArduinoID,SensorID,SensorValue
        Serial.print(ARDUINO_ID);
        Serial.print(",2,");  //SensorID from database
        Serial.print(h);    //SensorValue in %
        Serial.println(",");
        delay(1000);
    }
    //TEMP- AND HUMIDITYSENSOR

    //DUST SENSOR
    dustDuration = pulseIn(dustPin, LOW);
    dustLowpulseoccupancy = dustLowpulseoccupancy+dustDuration;
    if ((millis()-dustStarttime) > dustSampletime_ms)//if the sampel time == 30s
    {
        dustRatio = dustLowpulseoccupancy/(dustSampletime_ms*10.0);  // Integer percentage 0=>100
        dustConcentration = 1.1*pow(dustRatio,3)-3.8*pow(dustRatio,2)+520*dustRatio+0.62; // using spec sheet curve
        //Format: ArduinoID,SensorID,SensorValue,LowpulseoccupancyTime,Rs/R0Ratio
        Serial.print(ARDUINO_ID);
        Serial.print(",3,");  //SensorID from database
        Serial.print(dustConcentration);  //SensorValue in ppm
        Serial.print(",");
        Serial.print(dustLowpulseoccupancy);  //lowpulseoccupancy in microseconds
        Serial.print(",");
        Serial.print(dustRatio);  //reflects on which level lowpulseoccupancy Time takes up the whole sample time
        Serial.println(",");
        dustLowpulseoccupancy = 0; //reset lowpulseoccupancy
        dustStarttime = millis();
    }
    //DUST SENSOR
    delay(10);
}
