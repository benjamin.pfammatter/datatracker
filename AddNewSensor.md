# Adding A New Sensor

```http://wiki.seeedstudio.com```
Navigate to the sensor you want to add on this page on the left: Grove -> Sensor
In this example we add a dust sensor: ```http://wiki.seeedstudio.com/Grove-Dust_Sensor/```
Read through the documentation on the seeed wiki page. Some sensors need a specific connector on the SensorsAadapter from the Arduino. 
As example, this dust sensor needs to be connected to D8 and work only at that port.

### Connect the sensor
Connect the sensor to the connector on the SensorsAdapter.
In this example I connect the dust sensor to D8 of the SensorsAdapter. 
Note down the port number of the arduino: in this example it's the arduino of port ```/dev/ttyACM1```.

### Add the sensor to the databse

IMPORTANT: This way will delete all data from the database. If there are unexported data in the logging table from the datalogger database -> export it first!

Connect to the raspberry pi with a laptop through the wireless access point (WAP): ```car-data-tracker```.
Open a terminal and connect to the pi:
```sh
$ ssh pi@10.0.0.1 -p 22
datatracker     //password
$ sudo nano /home/pi/datatracker/mysql/database.sql
```
The file with all the database info will be opened. Scroll down to where the arduino data is added:
```INSERT INTO arduino (name, port, description)``` and note down the ArduinoID from the arduino connected to the port your using (/dev/ttyACM1).
In our case, the ArduinoID is: ```2```.


Scroll down to where the sensors data is added:
```INSERT INTO sensor (id, name, unit, description)``` and add a line below with a new SensorID, Name, Unit, Description:
```sh
(3, "Staubkonzentration", "ppm", "Grove-Dust Sensor V1.0"),
```
NOTE: SensorID must be a number never used by another sensor. 

Then scroll further down to where arduino_sensor data is added:
```INSERT INTO arduino_sensor (arduino, sensor)``` and add a line below with the ArduinoID the sensor is connected to and the SensorID defined previously
```sh
(2, 3),
```
ArduinoID: 2 (/dev/ttyACM1), SensorID: 3 (Staubkonzentration, added above).
Close and save the file by pressing ```Ctrl+X```and ```y```and ```Enter```.
Now we need to send this new data to the database (In this step everything in the database will be deleted!):
```sh
$ mysql -u root -p mysql
datatracker     //password
source /home/pi/datatracker/mysql/database.sql;
exit
```
Check if the new sensor is added to the database by visiting the address ```10.0.0.1/phpmyadmin```. Log in with username: ```root``` 
and password ```datatracker```. Open the database ```datalogger``` and check the tables ```sensor``` and ```arduino_sensor``` within and check if the new data is there as added above!

### Add the code to arduino
First, copy the header of the example code at ```http://wiki.seeedstudio.com/Grove-Dust_Sensor/``` and paste it to /home/pi/datatracker/arduino/ACM0/sensorsACM1.ino above ```void setup()```. 
Edit the code that it looks like the following:
```sh
//DUST SENSOR
int dustPin = 8;
unsigned long dustDuration;
unsigned long dustStarttime;
unsigned long dustSampletime_ms = 30000;//sampe 30s ;
unsigned long dustLowpulseoccupancy = 0;
float dustRatio = 0;
float dustConcentration = 0;
//DUST SENSOR
```

Then copy the code from ```http://wiki.seeedstudio.com/Grove-Dust_Sensor/``` which is in the ```void setup()``` function and paste it to ```/home/pi/datatracker/arduino/ACM0/sensorsACM1.ino``` inside ```void setup()```.
Edit the code that it looks like the following (there is only one ```Serial.begin(9600);```needed if there is already one):
```sh
//DUST SENSOR
pinMode(dustPin,INPUT);
dustStarttime = millis();//get the current time;
//DUST SENSOR
```

Then copy the code from ```http://wiki.seeedstudio.com/Grove-Dust_Sensor/``` which is in the ```void loop()``` function and paste it to ```/home/pi/datatracker/arduino/ACM0/sensorsACM1.ino``` inside ```void loop()```.
Edit the code that it looks like the following:
```sh
//DUST SENSOR
dustDuration = pulseIn(dustPin, LOW);
dustLowpulseoccupancy = dustLowpulseoccupancy+dustDuration;
if ((millis()-dustStarttime) > dustSampletime_ms)//if the sampel time == 30s
{
    dustRatio = dustLowpulseoccupancy/(dustSampletime_ms*10.0);  // Integer percentage 0=>100
    dustConcentration = 1.1*pow(dustRatio,3)-3.8*pow(dustRatio,2)+520*dustRatio+0.62; // using spec sheet curve
    Serial.print(ARDUINO_ID);
    Serial.print(",3,");  //SensorID from database
    Serial.print(dustConcentration);  //SensorValue in ppm
    Serial.print(",");
    Serial.print(dustLowpulseoccupancy);  //lowpulseoccupancy in microseconds
    Serial.print(",");
    Serial.print(dustRatio);  //reflects on which level lowpulseoccupancy Time takes up the whole sample time
    Serial.println(",");
    dustLowpulseoccupancy = 0; //reset lowpulseoccupancy
    dustStarttime = millis();
}
//DUST SENSOR
```
Save the file ```/home/pi/datatracker/arduino/ACM0/sensorsACM1.ino``` and close it.

### Upload the code to the arduino
Execute in a terminal connected to the raspberry pi:
```sh
$ cd /home/pi/datatracker/
$ ./arduinoUploader.sh
```
No password needed, just press enter. The above lines updates the code from all arduinos.
If everything works as expected, the arduino ACM1 should now log data from the new added dust sensor.

### Show new sensor data on the sensor dashboard
Visit the address ```10.0.0.1``` if connected to the WAP. There you see the sensor dashboard with all sensors.
Your new sensor should be displayed too, but with no data at the moment and with the wrong range and unit. We need to change that:

Open the file ```/var/www/html/js/gauge.js```. This is the file where we can set the range and unit of a new sensor.
Copy the code from another sensor (as example from the humidity sensor) which looks like this:
```sh
var humidity_options = {
	    credits: {
	        enabled: false
	    },
	    yAxis: {
	        min: 0,
	        max: 100,
	        title: { text: 'Feuchtigkeit' }
	    },
	    series: [{
	        name: 'Humidity',
	        data: [-30],
	        dataLabels: {
	            format: '<div style="text-align:center"><span style="font-size:25px;color:' +
	                	((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y}</span><br/>' +
	                   '<span style="font-size:12px;color:silver">%</span></div>'
	        },
	        tooltip: { valueSuffix: ' %' }
	    }]
	};
```
and paste it just below that code. Edit it (Unit, Title, Range) that it looks like this:
```sh
var dust_options = {
	    credits: {
	        enabled: false
	    },
	    yAxis: {
	        min: 0,
	        max: 15000,
	        title: { text: 'Staubkonzentration' }
	    },
	    series: [{
	        name: 'DustConcentration',
	        data: [-30],
	        dataLabels: {
	            format: '<div style="text-align:center"><span style="font-size:25px;color:' +
	                	((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y}</span><br/>' +
	                   '<span style="font-size:12px;color:silver">ppm</span></div>'
	        },
	        tooltip: { valueSuffix: ' ppm' }
	    }]
	};
```

Then navigate to the following line in the same file:
```sh
var chart = Highcharts.chart(containerId, Highcharts.merge(gaugeOptions,
```
We need to edit the following line to somethin similar to this (this looks different if there are other sensors, an example is in the file, as a comment, just below the line mentioned above):
```sh
(sensorType == 1 ? temperature_options : (sensorType == 2 ? humidity_options : dust_options) )
```

Reboot raspberry pi. In a terminal connected to it, execute:
```sh
$ sudo reboot
```

Save and close the file. Reload the page with deleting the cash (because otherwise the javascripts will not be reloaded) and the unit, title and range of the sensor should now be correct on the dashboard.
To display data of the new sensor start tracking data by clicking on the button at the top of the page ```Datenaufzeichnung Starten```, wait about a minute and click it again to stop. 

Hurra!!!!! You should see new data of the added sensor!
