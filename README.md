# Step-By-Step Raspberry Pi Installation Guide
This Step-By-Step guide tells you how to set up your Raspberry Pi to track sensor data. The sensors are connected to arduinos which are connected via USB to the Raspberry Pi.

Start: 20. November 2018

### Overview: 
- [OS](#install-os) (Raspian Stretch Lite)
- [LAMP](#install-lamp) (Linux, Apache, MySQL, PHP)
    - [Apache 2](#install-apache) (Webserver)
    - [MySQL](#install-mysql) (Database)
    - [PHP](#install-php)
    - [phpMyAdmin](#install-phpmyadmin)
        - [Change root user password](#change-phpmyadmin-root-user-password) (phpMyAdmin)
        - [New root user](#phpmyadmin-new-root-user) (phpMyAdmin)
        - [New user for logging](#create-new-user-for-data-logging) (phpMyAdmin)
- [Git](#install-git) (Backup)
    - [Datatracker](#datatracker)
        - [New Push](#new-datatracker-push)
    - [Webserver](#webserver)
        - [New Push](#new-webserver-push)
- [Access Point](#setup-access-point) (Local Network to connect without a WiFi)
- [pip](#install-pip-and-pip-libraries) (helps to install python libraries quickly)
- [Arduino IDE](#install-arduino-ide) (installs arduino suite and make files to flash the arduinos from the Raspberry Pi)
- [Backup Image](#backup)

## Install OS
Download boot Image: http://downloads.raspberrypi.org/raspbian/images/raspbian-2018-10-11/.
*Image Used: Raspbian Stretch Lite. It is the minimized version of the Raspbian OS, uses less memory because no visual desktop is included. For this project we need no desktop, we control everything from a terminal.*

Download Etcher and use it to flash the boot image to a microSD card: https://www.balena.io/etcher/

Plug the flashed microSD into your raspberry pi, connect to a monitor and ethernet and login with the following standard credentials:
```sh
Username: pi
Password: raspberry
```
*Attention: Y is Z on the keyboard (wrong keyboard settings by default).*
- Activate SSH:
```sh
$ passwd
raspberry
datatracker
datatracker
$ sudo raspi-config
```
- Navigate with the arrow keys to Interface Options and activate SSH. Changed Password in same menu to:
```sh
Username: pi
Password: datatracker
```
*You can now unplug the monitor, from now on everything can be done from an external terminal (for example your laptop). Search google to see how to connect to the Pi via SSH.*
## Install LAMP
LAMP: Linux, Apache, MySQL, PHP
- Apache is used to run a webserver on the Raspberry Pi to display sensor data on a website.
- MySQL is a database library which is used to store and access all sensor data.
- PHP is used to give logic to your website and compute data which is read from the database.

I used this guide to install LAMP, but described the exact steps in the following sections: https://pchelp.ricmedia.com/setup-lamp-server-raspberry-pi-3-complete-diy-guide/
### Install Apache
```sh
$ sudo apt-get update
$ sudo apt-get upgrade -y
$ sudo apt-get install apache2 -y
$ sudo a2enmod rewrite
$ sudo service apache2 restart
$ sudo nano /etc/apache2/apache2.conf
```
A text file should now be opened and we need to change the following: ```AllowOverride None``` to ```AllowOverride All``` under ```<Directory /var/www/>```. Then quit with ```Ctrl+X``` and press ```Y``` to save.
Restart apache webserver:
```sh
$ sudo service apache2 restart
```
Test if Apache server is running:
```sh
$ ifconfig
```
In my home network the pi had the IP-Adress ```192.168.1.108``` (connected per LAN).
Copy the IP-Address, go to a browser and add the port ```:80``` at the end: ```192.xxx.x.xxx:80```
You should see a website with some apache instructions.
### Install PHP
```sh
$ sudo apt-get install php libapache2-mod-php -y
$ cd /var/www/html
$ sudo nano index.php
```
An empty text file should now be opened. Paste: ```<?php echo "Hello World"; ?>```.
Quit with ```Ctrl+X``` and save with ```Y```.
Delete the index.html file and restart apache:
```sh
$ sudo rm index.html
$ sudo service apache2 restart
```
Check again in a browser: ```192.xxx.x.xxx:80```. You should see an empty website with the text: ```Hello World```.
### Install MySQL
```sh
$ sudo apt-get install mysql-server php-mysql -y
$ sudo service apache2 restart
```
### Install phpMyAdmin
In order to manage our MySQL databases, it’s far easier to use phpMyAdmin so let’s go ahead and install it by entering the following command, but note* you’ll be asked the following questions during the installation process:
- Automatic Configuration? – Choose [*]apache2 with your Spacebar, hit ```Tab``` and then ```Enter```
- Configure database for phpmyadmin with dbconfig-common? – Choose Yes
- phpMyAdmin application password – [enter new password: ```datatracker```] [confirm new password: ```datatracker```] this is for logging into the phpMyAdmin web interface.

*Note: Now phpMyAdmin password: ```datatracker``` for user ```phpmyadmin```*
```sh
$ sudo apt-get install phpmyadmin -y
```
Check in again in a browser: ```192.xxx.x.xxx:80/phpmyadmin``` if you can log into phpMyAdmin:
```sh
Username: phpmyadmin
Password: datatracker
```
#### Change phpMyAdmin root user password
Guide: https://blog.dotkam.com/2007/04/10/mysql-reset-lost-root-password/
```sh
$ sudo su
```
Change root password:
```sh
$ passwd
datatracker
```
*Note: New Raspberry Pi password: ```datatracker``` for user ```root```!*
```sh
$ ps -ef | grep mysql
$ pkill mysqld
$ mysqld_safe --skip-grant-tables &
mysql -u root mysql
UPDATE user SET password=PASSWORD("datatracker") WHERE user="root";
FLUSH PRIVILEGES;
exit
$ pkill mysqld_safe
$ /etc/init.d/mysql start
```
*Note: New phpMyAdmin password: ```datatracker``` for user ```root```!*
Test the new password (still as su=super user):
```sh
$ mysql -u root -p mysql
datatracker
```
Setup a New Database with an .sql file (safe the database.sql file at the path ```/home/pi/datatracker/mysql/database.sql```). 
*Note: Every time you want to reset your database you can repeat these commands (with the login above)*
```sh
source /home/pi/datatracker/mysql/database.sql;
exit
```
#### phpMyAdmin new root user
Change Privileges and password of phpMyAdmin ```root``` user to login over a browser:
```sh
$ sudo mysql -u root
SELECT User,Host FROM mysql.user;
```
Output should be something similar to this:
```sh
+------------+-----------+
| User       | Host      |
+------------+-----------+
| root       | localhost |
| phpmyadmin | localhost |
+------------+-----------+
```
Delete current root user and recreate with password ```datatracker```:
```sh
mysql> DROP USER 'root'@'localhost';
mysql> CREATE USER 'root'@'%' IDENTIFIED BY 'datatracker';
mysql> GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' WITH GRANT OPTION;
mysql> FLUSH PRIVILEGES;
mysql> exit
```
Now we can log into phpMyAdmin root user without sudo and from an external browser.
#### Create new user for data logging
Username: ```datalogger```
Password: ```datalogger```
The new user has only all privileges at the ```datalogger``` database created before with the .sql file.
```sh
$ mysql -u root -p
datatracker
mysql> CREATE USER 'datalogger'@'%' IDENTIFIED BY 'datalogger';
mysql> GRANT ALL PRIVILEGES ON datalogger.* TO 'datalogger'@'%';
mysql> FLUSH PRIVILEGES;
mysql> exit
```
Test to login at ```192.xxx.x.xxx/phpmyadmin``` with the new username ```datalogger``` and password ```datalogger```.
If all works correctly you should only see the databases ```datalogger``` and ```information_schema```.

Congratulation, you set up your raspberry pi successfully with LAMP and a working datalogger database!
## Install Git
Git is used to have an online backup of all important files and you can restore older versions of all those files. In this guide I will use the git client GitLab which is free and easy to install.
TWO git repositories will be installed. One repository for the folder ```/home/pi/datatracker/``` where all the arduino files, etc. are located. And another repository for the folder ```/var/www/``` where all the files for the webserver all located.
```sh
$ sudo apt-get update
$ sudo apt-get install git
```
### Datatracker
Execute the following commands in the folder ```/home/pi/datatracker/```. User your own credentials of your gitlab account and the project: ```user.name```, ```user.email``` and ```origin```.
```sh
$ cd /home/pi/datatracker/
$ nano .gitignore
!.gitignore             //then close and save (Ctrl+C, y, Enter)
$ git init
$ git config user.name "Benjamin Pfammatter"
$ git config user.email "benjamin.pfammatter@gmail.com"
$ git remote add origin git@gitlab.com:benjamin.pfammatter/datatracker.git
$ git add .
$ git commit -m "Initial Commit"
```
Generate an SSH-Key and save in your profile settings at gitlab.com
```sh
$ ssh-keygen -t rsa -C "benjamin.pfammatter@gmail.com" -b 4096
```
Then Enter, Enter, Enter (Path, No Password, No Password).
Copy the content of the file ```/home/pi/.ssh/id_rsa.pub``` with FTP or something similar (pi user) and paste by making a new SSH-Key in GitLab-Profile-Settings. If the ssh key is working correctly you should be able to push all files from the folder ```/home/pi/datatracker/``` to GitLab:
```sh
$ git push -u origin master
```
Now, the folder ```/home/pi/datatracker/``` should be uploaded to GitLab.
#### New Datatracker Push
Every time you made a few changes in the ```/home/pi/datatracker/``` folder you should update your git repository:
*execute at ```/home/pi/datatracker/```*
```sh
$ cd /home/pi/datatracker/
$ git pull
$ git add .
$ git commit -m "Beschreibung der Änderungen"
$ git push -u origin master
```
### Webserver
Execute the following commands in the folder ```/var/www/```
```sh
$ cd /var/www/
$ sudo adduser pi www-data
$ sudo chown -R www-data:www-data /var/www
$ sudo chmod -R g+rwX /var/www
$ sudo nano .gitignore
!.gitignore         //then close and save (Ctrl+C, y, Enter)
$ sudo git init
$ sudo git config user.name "Benjamin Pfammatter"
$ sudo git config user.email "benjamin.pfammatter@gmail.com"
$ sudo git remote add origin git@gitlab.com:benjamin.pfammatter/datatrackerwebserver.git
$ sudo git add .
$ sudo git commit -m "Initial Commit"
```
Again, generate an SSH-Key and save at GitLab
```sh
$ sudo ssh-keygen -t rsa -C "benjamin.pfammatter@gmail.com" -b 4096
```
Then Enter, Enter, Enter (Path, No Password, No Password).
Copy the content of the file ```/root/.ssh/id_rsa.pub``` with FTP or something similar (root user) and paste by making a new SSH-Key in GitLab-Profile-Settings. Then:
```sh
$ sudo git push -u origin master
```
Now, the folder ```/var/www/``` should be uploaded to GitLab.
#### New Webserver Push
*execute at ```/var/www/```*
```sh
$ cd /var/www/
$ sudo git pull
$ sudo git add .
$ sudo git commit -m "Beschreibung der Änderungen"
$ sudo git push -u origin master
```
## Setup Access Point
Make an access point to connect locally (without a WiFi around) to the Raspberry Pi:
- to display the webserver on another device (tablet, laptop, phone, etc.)
- to control the pi via ssh

Guide: https://raspberrypi.stackexchange.com/questions/78978/setting-up-an-accesspoint-on-the-raspberry-pi-3-with-debian-stretch (Answer 1)
```sh
$ curl -sSL https://gist.github.com/Lewiscowles1986/fecd4de0b45b2029c390/raw/0c8b3af3530a35db9ab958defe9629cb5ea99972/rPi3-ap-setup.sh | sudo bash $0 datatracker car-data-tracker
$ sudo wget -q https://gist.githubusercontent.com/Lewiscowles1986/390d4d423a08c4663c0ada0adfe04cdb/raw/5b41bc95d1d483b48e119db64e0603eefaec57ff/dhcpcd.sh -O /usr/lib/dhcpcd5/dhcpcd
$ sudo chmod +x /usr/lib/dhcpcd5/dhcpcd
```
Worked perfectly (Raspberry Pi 3 B+, Raspbian Stretch, 20. November 2018).
SSID: ```car-data-tracker```
WPA password: ```datatracker```
If you want to change SSID, WPA password or some other settings edit the following file:
```sh
$ sudo nano /etc/hostapd/hostapd.conf
```
If eth0 (Ethernet) should not work add this to bottom of the file:
```sh
$ sudo nano /etc/network/interfaces
auto eth0
    allow-hotplug eth0
    iface eth0 inet dhcp
```
## Install pip and pip-libraries
Tool to install python libraries:
```sh
$ sudo apt-get install python-pip
```
Intall MySQL Connector through pip to be able to access the mysql database from python scripts
```sh
$ pip install mysql-connector-python
```
## Install Arduino IDE
Install Arduino IDE to flash code to the arduino from Raspberry Pi, connected via USB.
```sh
$ sudo apt-get install arduino
$ sudo apt-get install arduino-mk
```
## Backup
### Backup Image of Raspberry Pi microSD card
Guide (Mac): https://maker-tutorials.com/rpi-backup-mac-terminal/
Replace ```/dev/disk*``` in the terminal commands below with the device that has the right memory size (your microSD card).
Replace ```rpi-image.gz``` with a filename you want.
```sh
$ diskutil list                     //Identify /dev/disk* based on memory size
$ sudo dd bs=4m if=/dev/disk* | gzip > ~/Desktop/rpi-image.gz
```
Maybe you will be asked to enter your password. This process can take up to a few HOURS! You will not see a progress bar so just wait until the process is complete.
### Recover Raspberry Pi microSD card from a made Backup Image
Guide (Mac): https://maker-tutorials.com/rpi-backup-mac-terminal/
Replace ```/dev/disk*``` with the device that has the right memory size.
Replace ```rpi-image.gz``` with the name of your backup file. The backup file should be placed on your desktop.
```sh
$ diskutil list
$ diskutil umountDisk /dev/disk*
$ sudo newfs_msdos -F 16 /dev/disk*
$ sudo gzip -dc ~/Desktop/rpi-image.gz | sudo dd bs=4m of=/dev/disk*
```
This process can take about twice or more as long as the backup process.

## Start python script at boot
Prepare the Raspberry Pi to start the readAndLog.py file at boot, do this only if the readAndLog.py file is already located at the path ```/var/www/html```.
```sh
$ sudo nano /etc/profile
```
At the bottom of the file add the following line and save with ```Ctrl+X``` then `Y`: 
```(sh /etc/startPythonAtBoot.sh)&```
Then, make a new shell-script file:
```sh
$ sudo nano /etc/startPythonAtBoot.sh
```
And insert the following and save with ```Ctrl+X``` then `Y` (this will start the readAndLog.py file at boot)
```sh
#!/bin/bash

cd /var/www/html
python readAndLog.py
echo "startedPythonFileSuccessfully"
```
This .sh file will start the python script everytime the Raspberry Pi boots up.

```sh
$ sudo reboot
```

*After all the steps above your Raspberry Pi is ready to use for tracking sensor data!*
*In the file COMMANDS.md you will find some useful commands to control your Raspberry Pi and your connected Arduinos*